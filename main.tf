terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~>3.13.0"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_token
}

module "module1" {
  source = "./module1"
}

output "yaml_contents" {
  value = module.module1.yaml_contents
}
