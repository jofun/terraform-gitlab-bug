module "module2" {
  source = "./module2"
}

output "yaml_contents" {
  value = module.module2.yaml_contents
}
