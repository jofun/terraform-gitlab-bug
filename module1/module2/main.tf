terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~>3.13.0"
    }
  }
}

data "gitlab_repository_file" "boop" {
  project   = "jofun/terraform-gitlab-bug"
  ref       = "main"
  file_path = "boop.yml"
}

output "yaml_contents" {
  value = yamldecode(base64decode(data.gitlab_repository_file.boop.content))
}
