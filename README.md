# terraform-gitlab-bug

This is a sample repo to verify if Gitlab's Terraform provider works as expected with nested child modules, using implicit inheritance to inherit the Gitlab provider token.

## Does it work?

Yes.

```
johanan@workstation (main) ~/code/terraform-gitlab-bug terraform plan -var-file vars.tfvars

Changes to Outputs:
  + yaml_contents = [
      + "Hey,",
      + "You",
      + "Found",
      + "Me!",
    ]

You can apply this plan to save these new output values to the Terraform state, without changing any real infrastructure.

────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Note: You didn't use the -out option to save this plan, so Terraform can't guarantee to take exactly these actions if you run
"terraform apply" now.
johanan@workstation (main) ~/code/terraform-gitlab-bug terraform apply -var-file vars.tfvars

Changes to Outputs:
  + yaml_contents = [
      + "Hey,",
      + "You",
      + "Found",
      + "Me!",
    ]

You can apply this plan to save these new output values to the Terraform state, without changing any real infrastructure.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: no

Apply cancelled.
johanan@workstation (main) ~/code/terraform-gitlab-bug
```

